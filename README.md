#  Clock for macOS
![Promo](./Promos/v1_promo.png)
This app strives to work as a good desktop alternative to iOS's Clock app.

## Functions
### World Clock
![World Clock](./Screenshots/clocks.png)
Users can set up a list of clocks based on different cities around the globe.
Unlike the iOS Clock app, world clocks' names are set manually by the user, instead of automatically being named after their target city. This means that the user can name a specific clock after a friend they have in a different time zone, for instance.

### Countdowns
![Countdowns](./Screenshots/countdowns.png)
Countdowns are a feature not available in the iOS Clock app. These count down to events on a specific date and time, and as such are great for things like birthdays or holidays.
The user can specify what behavior a given countdown should exhibit once its date and time passes:
* **Show Zero** - When the countdown passes zero, it should show all zeroes.
* **Show Time Elapsed** - When the countdown passes zero, it should begin to count up, showing the amount of time that has passed since the countdown's date and time.
* **Repeat** - When the countdown passes zero, it should automatically reschedule the countdown:
    * **Yearly** - Reschedules the countdown for next year.
    * **Monthly** - Reschedules the countdown for next month.
    * **Weekly** - Reschedules the countdown for next week.
    * **Daily** - Reschedules the countdown for tomorrow.
    
    
### Timers
![Timers](./Screenshots/timers.png)
Timers work fairly almost identically to those in the iOS Clock app. Users can set a given number of hours, minutes, and seconds for a timer, and will receive a notification once the timer finishes. Unlike in iOS, multiple timers can be set at once. At present, the timer's end-behavior cannot be customized.

### Alarms
![Alarms](./Screenshots/alarms.png)
Alarms can be set up with a given name, time of day to ring, and days of the week to ring on. In addition, they can be deactivated if the user does not want to use them. Like timers, their ring behavior (sounds, etc) cannot be customized, and there is no "snooze" functionality implemented yet.


### Stopwatch
![Stopwatch](./Screenshots/stopwatch.png)
A single stopwatch can be started, paused, resumed, and cleared. While the stopwatch is running, laps can also be recorded. Individual laps show their duration, as well as the difference in time between them and the previous lap. The longest lap recorded is displayed in red, and the shortest lap in green.

## Roadmap/Future Features
These are features that I'd like to implement in the future. If you have features you'd like to see implemented, or bugs you'd like to see fixed, please feel free to [add them as a new issue](https://gitlab.com/Meorge/Clock/-/issues)!
* World Clock
    * Analog clock view for world clocks
    * World map view for world clocks (à la Clock on iPad)
* Countdowns
    * Notification when countdown finishes
* Timers
    * Make timer-setting UI more friendly
    * Allow user to set notification sound
* Alarms
    * Allow user to set notification sound
* General
    * Improve performance
    
## Licenses
Clock for macOS is licensed under the GNU GPL v3.
### TimeZonePicker
This app uses a version of [Gligor Kotushevski's TimeZonePicker](https://github.com/gligorkot/TimeZonePicker), [adapted to SwiftUI by me](https://github.com/Meorge/TimeZonePicker). It is licensed under the Apache-2.0 license:
```
Copyright (c) 2020 Gligor Kotushevski, 2020 Malcolm Anderson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
