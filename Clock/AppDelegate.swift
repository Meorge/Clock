//
//  AppDelegate.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/2/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import Cocoa
import SwiftUI
import Combine
import UserNotifications

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    let window: NSWindow = {
        let window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 800, height: 500),
            styleMask: [.titled, .closable, .miniaturizable, .resizable],
            backing: .buffered, defer: false)
        window.isReleasedWhenClosed = false
        return window
    }()
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // build the other window stuff
        self.buildMainWindow()
    }
    
    func buildMainWindow() {
        print(TimeZoneDataManager.sharedInstance)
        // Create the SwiftUI view that provides the window contents.
        let contentView = ContentView()
            .environmentObject(TimerManager.instance)
        
        // set up the window
        window.center()
        window.titleVisibility = .hidden
        
        window.toolbar = NSToolbar()

        window.contentView = NSHostingView(rootView: contentView)
        
        window.makeKeyAndOrderFront(self)
        
        
    }
    
    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        // check our alarm timers
        
        print("Timers")
        for i in TimerManager.instance.alarmTimers {
            print("\(i.key) - date=\(i.value.fireDate), valid=\(i.value.isValid)")
        }
        print("----")
        
        if flag == false {
            window.makeKeyAndOrderFront(self)
            return true
        }
        return false
    }
    
//    func applicationDidBecomeActive(_ notification: Notification) {
//        <#code#>
//    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}
