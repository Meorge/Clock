//
//  WorldClockView.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/24/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct WorldClockView: View {
    @EnvironmentObject var timerHolder: TimerManager
    var body: some View {
        List {
            ForEach(self.timerHolder.clocks, id: \.id) { clock in
                WorldClockRow(clockItem: clock)
            }
                .onDelete(perform: self.timerHolder.deleteWorldClockItem)
                .onMove(perform: self.timerHolder.moveWorldClockItem)
            NewWorldClockRow()
        }
        
        
    }
}

struct WorldClockView_Previews: PreviewProvider {
    static var previews: some View {
        WorldClockView()
    }
}
