//
//  AddWorldClockModal.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/24/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct AddWorldClockModal: View {
    @EnvironmentObject var timerManager: TimerManager
    @Environment(\.presentationMode) var presentationMode
    
    @State var worldClockItem: WorldClockItem = WorldClockItem()
    @State var createNew: Bool = true
    var body: some View {
        Form {
            TextField("Name", text: $worldClockItem.name)
            TimeZonePreviewSelectionView(cityItem: self.$worldClockItem.cityCountryTimeZone)
            HStack {
                Spacer()
                Button(action: cancel) { Text("Cancel") }
                Button(action: createClock) { Text("Add Clock") }
            }
        }
    }
    
    func cancel() {
        self.presentationMode.wrappedValue.dismiss()
    }
    
    func createClock() {
        if createNew {
            self.timerManager.addWorldClockItem(worldClockItem)
        } else {
            self.timerManager.updateWorldClockDetails(for: worldClockItem)
        }
        self.presentationMode.wrappedValue.dismiss()
    }
}

struct AddWorldClockModal_Previews: PreviewProvider {
    static var previews: some View {
        AddWorldClockModal()
    }
}
