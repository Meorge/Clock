//
//  WorldClockItem.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/24/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import Foundation

struct WorldClockItem: Codable, Equatable, Hashable {
    var id = UUID()
    
    public var name = "Clock"
    public var cityCountryTimeZone: CityCountryTimeZone? {
        didSet {
            timeZone = TimeZone.init(identifier: cityCountryTimeZone!.timeZoneName)!
            print("Set up time zone to be with identifier \(cityCountryTimeZone!.timeZoneName)")
            print("Time zone: \(timeZone.abbreviation() ?? "unknown")")
        }
    }
    public var timeZone = TimeZone.current
    
    func getTimeString() -> String {
        let dF = DateFormatter()
        dF.dateFormat = "hh:mm a"
        dF.timeZone = timeZone
        return dF.string(from: Date())
    }
    
    func getTimeDifferenceString() -> String {
        var stringOut = "\(timeZone.abbreviation() ?? "???")"
        let hrsDiff = (timeZone.secondsFromGMT() - TimeZone.current.secondsFromGMT()) / 3600
        stringOut += ", \(hrsDiff >= 0 ? "+" : "")\(hrsDiff)HRS"
        
        return stringOut
    }
}
