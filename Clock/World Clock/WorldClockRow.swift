//
//  WorldClockRow.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/24/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct WorldClockRow: View {
    @State var popoverEnabled: Bool = false
    @EnvironmentObject var timerManager: TimerManager
    var clockItem: WorldClockItem = WorldClockItem()
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(clockItem.name)
                    .font(.headline)
                    .fontWeight(.semibold)
                Text(clockItem.getTimeDifferenceString())
            }
            Spacer()
            Text(clockItem.getTimeString())
                .font(Font.system(.title).monospacedDigit())
        }
        .sheet(isPresented: $popoverEnabled) {
            AddWorldClockModal(worldClockItem: self.clockItem, createNew: false)
                .padding(.all, 10.0)
                .environmentObject(self.timerManager)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            self.popoverEnabled.toggle()
        }
    }
}

struct NewWorldClockRow: View {
    @State var popoverEnabled: Bool = false
    
    @EnvironmentObject var timerManager: TimerManager
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Add a new clock...")
                    .font(.headline)
                    .foregroundColor(Color.gray)
                    
                Text("")
            }
            Spacer()
        }
        .sheet(isPresented: $popoverEnabled) {
            AddWorldClockModal(worldClockItem: WorldClockItem(), createNew: true)
                .padding(.all, 10.0)
                .environmentObject(self.timerManager)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            self.popoverEnabled.toggle()
        }
    }
}

struct WorldClockRow_Previews: PreviewProvider {
    static var previews: some View {
        WorldClockRow(clockItem: WorldClockItem())
    }
}
