//
//  TimerManager.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/6/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI
import Combine
import UserNotifications

// from https://stackoverflow.com/questions/56895459/getting-unresolved-identifier-self-in-swiftui-code-trying-to-use-timer-schedul/56905649#56905649
class TimerManager : ObservableObject {
    static let instance = TimerManager()
    var dateFormatter : DateFormatter = DateFormatter()
    
    let timer = Timer.publish(every: 0.01, on: .main, in: .common).autoconnect()
    var timerCancellable: AnyCancellable?
    
    @Published var countdowns: [CountdownItem] = []
    @Published var timers: [TimerItem] = []
    
    @Published var timerOriginDate: Date = Date()
    
    @Published var stopwatchItem: StopwatchItem = StopwatchItem()
    
    @Published var alarms: [AlarmItem] = []
    var alarmTimers: [UUID : Timer] = [:]
    
    @Published var clocks: [WorldClockItem] = []
    

    
    private init() {
        self.dateFormatter.dateStyle = .full
        self.dateFormatter.timeStyle = .short
        self.timerCancellable = self.timer.sink(receiveValue: self.updateTimer)
        
//        self.clearData()
        self.loadData()
        print("TimerManager initialized!")
    }
    
    deinit {
        print("Deinitializing TimerManager")
    }
    
    func updateTimer(date: Date) {
        self.updateAllCountdowns()
        self.updateAllTimers()
        self.stopwatchItem.update()
    }
    
    func clearData() {
        UserDefaults.standard.removeObject(forKey: "countdowns")
        UserDefaults.standard.removeObject(forKey: "timers")
        UserDefaults.standard.removeObject(forKey: "alarms")
        UserDefaults.standard.removeObject(forKey: "worldClocks")
    }
    
    func loadData() {
        self.countdowns = []
        if let data = UserDefaults.standard.data(forKey: "countdowns") {
            if let decoded = try? JSONDecoder().decode([CountdownItem].self, from: data) {
                self.countdowns = decoded
            }
        }
        
        self.timers = []
        if let data = UserDefaults.standard.data(forKey: "timers") {
            if let decoded = try? JSONDecoder().decode([TimerItem].self, from: data) {
                self.timers = decoded
            }
        }
        
        self.alarms = []
        if let data = UserDefaults.standard.data(forKey: "alarms") {
            if let decoded = try? JSONDecoder().decode([AlarmItem].self, from: data) {
                self.alarms = decoded
            }
        }
        
        if self.alarms.count >= 1 {
            for i in 0...(self.alarms.count - 1) {
                self.alarms[i].date = nextAlarmOccurrence(for: self.alarms[i])
                attemptScheduleAlarmNotification(for: self.alarms[i])
            }
        }
        self.clocks = []
        if let data = UserDefaults.standard.data(forKey: "worldClocks") {
            if let decoded = try? JSONDecoder().decode([WorldClockItem].self, from: data) {
                self.clocks = decoded
            }
        }
        
    }
    
    func saveData() {
        if let encoded = try? JSONEncoder().encode(self.countdowns) {
            UserDefaults.standard.set(encoded, forKey: "countdowns")
        }
        
        if let encoded = try? JSONEncoder().encode(self.timers) {
            UserDefaults.standard.set(encoded, forKey: "timers")
        }
        
        if let encoded = try? JSONEncoder().encode(self.alarms) {
            UserDefaults.standard.set(encoded, forKey: "alarms")
        }
        
        if let encoded = try? JSONEncoder().encode(self.clocks) {
            UserDefaults.standard.set(encoded, forKey: "worldClocks")
        }
    }
    
    func addCountdown(item: CountdownItem) {
        self.countdowns.append(item)
        self.saveData()
    }
    
    func deleteCountdown(at offsets: IndexSet) {
        self.countdowns.remove(atOffsets: offsets)
        self.saveData()
    }
    
    func addTimer(hours: Int, minutes: Int, seconds: Int) {
        let newTimer = TimerItem(hours: Double(hours), minutes: Double(minutes), seconds: Double(seconds))
        self.timers.append(newTimer)
        self.saveData()
    }
    
    func deleteTimer(at offsets: IndexSet) {
        self.timers.remove(atOffsets: offsets)
        self.saveData()
    }
    
    func moveTimerItem(from source: IndexSet, to destination: Int) {
        self.timers.move(fromOffsets: source, toOffset: destination)
        self.saveData()
    }
    
    func togglePauseTimer(_ timer: TimerItem) {
        if let index = self.timers.firstIndex(of: timer) {
            var newTimer = timer
            newTimer.togglePause()
            self.timers[index] = newTimer
            self.saveData()
        } else {
            print("couldn't find the index of the timer item :(")
        }
    }
    
    func updateCountdownDetails(original: CountdownItem, new: CountdownItem) {
        print("updateCountdownDetails()")
        print(original)
        print(new)
        
        print("all countdowns:")
        for c in self.countdowns {
            print(c)
        }
        print("------")
        
        var index = -1
        for c in self.countdowns {
            if c.id == original.id {
                index = self.countdowns.firstIndex(of: c)!
                break
            }
        }
        
        if index >= 0 {
            self.countdowns[index] = new
            self.saveData()
        }
        else {
            print("Couldn't find index of countdown item :(")
        }
    }
    
    func moveCountdown(from source: IndexSet, to destination: Int) {
        print("do some movin")
        self.countdowns.move(fromOffsets: source, toOffset: destination)
        self.saveData()
    }
    
    func updateAllCountdowns() {
        if (self.countdowns.count == 0) {
            return
        }
        for index in 0...self.countdowns.count - 1 {
            self.countdowns[index].update()
        }
    }
    
    func updateAllTimers() {
        if (self.timers.count == 0) {
            return
        }
        for index in 0...self.timers.count - 1 {
            self.timers[index].update()
        }
    }
    
    func toggleStopwatch() {
        if (self.stopwatchItem.isFresh()) {
            print("Fresh stopwatch!")
            self.stopwatchItem.start()
        }
        
        else {
            if (self.stopwatchItem.active) { self.stopwatchItem.stop() }
            else { self.stopwatchItem.resume() }
        }
    }
    
    func startStopwatch() {
        self.stopwatchItem.start()
    }
    
    func getUnitString(unit: Calendar.Component) -> String {
        switch unit {
            case .day:
                return "days"
            case .hour:
                return "hours"
            case .minute:
                return "mins"
            case .second:
                return "secs"
            default:
                return "???"
        }
    }
    
    
    func addAlarm(_ item: AlarmItem) {
        var item = item
        item.date = TimerManager.instance.nextAlarmOccurrence(for: item)
        attemptScheduleAlarmNotification(for: item)
        self.alarms.append(item)
        self.saveData()
    }
    
    func deleteAlarm(at offsets: IndexSet) {
        let un = UNUserNotificationCenter.current()
        for i in offsets {
            let alarm = self.alarms[i]
            un.removePendingNotificationRequests(withIdentifiers: [alarm.id.uuidString])
            un.removeDeliveredNotifications(withIdentifiers: [alarm.id.uuidString])
            self.alarmTimers[alarm.id]?.invalidate()
            self.alarmTimers.removeValue(forKey: alarm.id)
        }
        self.alarms.remove(atOffsets: offsets)
        self.saveData()
    }
    
    func moveAlarmItem(from source: IndexSet, to destination: Int) {
        self.alarms.move(fromOffsets: source, toOffset: destination)
        self.saveData()
    }
    
    func updateAlarmDetails(for item: AlarmItem) {
        var item = item
        item.date = TimerManager.instance.nextAlarmOccurrence(for: item)
        attemptScheduleAlarmNotification(for: item)
        
        print("Alarm named \(item.name) will fire next on \(item.getNextFireString())")
        var index = -1
        for c in self.alarms {
            if c.id == item.id {
                index = self.alarms.firstIndex(of: c)!
                break
            }
        }
        
        if index >= 0 {
            self.alarms[index] = item
            self.saveData()
        }
        else {
            print("Couldn't find index of alarm item :(")
        }
    }
    
    @objc func updateAlarm(timer: Timer) {
        let alarm = timer.userInfo as! AlarmItem
        print("Update alarm \(alarm.name)")
        updateAlarmDetails(for: alarm)
    }
    
    func nextAlarmOccurrence(for alarm: AlarmItem) -> Date {
        var date = alarm.date
        
        // get the minute and hour of the alarm
        let hour = Calendar.current.component(.hour, from: date)
        let minute = Calendar.current.component(.minute, from: date)
        
        // apply it to today
        date = Calendar.current.date(bySettingHour: hour, minute: minute, second: 0, of: Date())!

        // determine if this alarm already should have gone off today - if so, we need to reschedule it for the next possible time!
        let timerAlreadyPassed = date <= Date()
        
        // Hacky and probably won't work??
        // The intent of this is to shove the timer forward a day so we can find the next occurrence of it
        if timerAlreadyPassed {
            date.addTimeInterval(TimeInterval(86400))
        }
        
        // find out what the current weekday is
        let currentWeekday = DayOfWeek(rawValue: Calendar.current.component(.weekday, from: date))
        
        // quickly make sure it's enabled for at least one day of the week, otherwise things will get funky
        if alarm.days.isEmpty {
            print("Alarm with name \(alarm.name) will never go off!")
            return Date()
        }
        // let's figure out what the next weekday is that's also allowed by this alarm
        var nextWeekday = currentWeekday
        var daysToAdd = 0
        while !alarm.days.contains(nextWeekday!) && !alarm.days.isEmpty {
            nextWeekday?.increment()
            daysToAdd += 1
        }
        
        // ok, let's now add the number of days to it
        date.addTimeInterval(TimeInterval(daysToAdd * 86400))
        
        print("Next alarm time determined for alarm \"\(alarm.name)\"")
        
        return date
    }
    
    func attemptScheduleAlarmNotification(for alarm: AlarmItem) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            if success {
                self.scheduleAlarmNotification(for: alarm)
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    func scheduleAlarmNotification(for alarm: AlarmItem) {
        let un = UNUserNotificationCenter.current()
        un.removePendingNotificationRequests(withIdentifiers: [alarm.id.uuidString])
        un.removeDeliveredNotifications(withIdentifiers: [alarm.id.uuidString])
        
        if !alarm.enabled {
            print("Alarm named \"\(alarm.name)\" isn't active, so don't schedule a notif for it")
            return
        }
        
        let notifContent = UNMutableNotificationContent()
        notifContent.title = alarm.name
        notifContent.body = "Alarm ringing"
        notifContent.sound = UNNotificationSound.default
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.hour, .minute], from: alarm.date), repeats: false)
        
        let request = UNNotificationRequest(identifier: alarm.id.uuidString, content: notifContent, trigger: trigger)
        
        un.add(request)
        
        let recalcNextOccurrenceTimer = Timer(fireAt: alarm.date, interval: 0, target: self, selector: #selector(updateAlarm), userInfo: alarm, repeats: false)
        
        RunLoop.main.add(recalcNextOccurrenceTimer, forMode: .common)
        
        self.alarmTimers[alarm.id]?.invalidate()
        self.alarmTimers[alarm.id] = recalcNextOccurrenceTimer
        
        print("Notification scheduled for alarm \(alarm.name) at date \(alarm.getNextFireString()) and time \(alarm.getAlarmTimeString())")
    }
    
    func addWorldClockItem(_ item: WorldClockItem) {
        self.clocks.append(item)
        self.saveData()
    }
    
    func deleteWorldClockItem(at offsets: IndexSet) {
        self.clocks.remove(atOffsets: offsets)
        self.saveData()
    }
    
    func updateWorldClockDetails(for item: WorldClockItem) {
        var index = -1
        for c in self.clocks {
            if c.id == item.id {
                index = self.clocks.firstIndex(of: c)!
                break
            }
        }
        
        if index >= 0 {
            self.clocks[index] = item
            self.saveData()
        }
        else {
            print("Couldn't find index of clock item :(")
        }
    }
    
    func moveWorldClockItem(from source: IndexSet, to destination: Int) {
        self.clocks.move(fromOffsets: source, toOffset: destination)
        self.saveData()
    }
    
}
