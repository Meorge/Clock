//
//  TimerRow.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/5/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct TimerRow: View {
    @EnvironmentObject var timerManager: TimerManager
    var timerItem: TimerItem
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
            Text(self.timerItem.getTimeRemaining())
                .font(Font.system(.title).monospacedDigit())
                .foregroundColor(self.timerItem.getRemainingSeconds() < 0 ? Color.gray : Color.primary)
            Text(self.timerItem.getEndTime())
            }
            Spacer()
            if (self.timerItem.getRemainingSeconds() > 0) {
                Button(action: { self.timerManager.togglePauseTimer(self.timerItem) }) {
                    Text(self.timerItem.active ? "Pause" : "Resume")
                }
            }
        }
    }
}

struct NewTimerRow: View {
    @State var popoverEnabled: Bool = false
    
    @EnvironmentObject var timerManager: TimerManager
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Add a new timer...")
                    .font(.headline)
                    .foregroundColor(Color.gray)
                    
                Text("")
            }
            Spacer()
        }
        .sheet(isPresented: $popoverEnabled) {
            AddTimerModal()
                .padding(.all, 10.0)
                .environmentObject(self.timerManager)
//            AddCountdownModal()
//                .padding(.all, 10.0)
//                .environmentObject(self.timerManager)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            self.popoverEnabled.toggle()
        }
    }
}

struct TimerRow_Previews: PreviewProvider {
    static var previews: some View {
        TimerRow(timerItem: TimerItem(hours: 0, minutes: 30, seconds: 0))
    }
}
