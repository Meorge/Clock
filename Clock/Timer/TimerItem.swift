//
//  TimerItem.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/5/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import Foundation
import UserNotifications

struct TimerItem: Codable, Equatable, Hashable {
    var id = UUID()
    
    public var startDate: Date
    public var endDate: Date
    public var active: Bool = true
    var notificationSent: Bool = false
    public var remaining: DateComponents = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date(), to: Date())

    init(hours: Double, minutes: Double, seconds: Double) {
        self.startDate = Date()
        self.endDate = Date().addingTimeInterval(seconds + (minutes * 60) + (hours * 3600))
        print("create new timer, hours=\(hours), minutes=\(minutes), seconds=\(seconds)")
        print("start date: \(self.startDate) end date: \(self.endDate)")
    }
    
    public func getTimeRemaining() -> String {
        let hourPart = String(format: "%02ld", abs(self.remaining.hour!))
        let minutePart = String(format: "%02ld", abs(self.remaining.minute!))
        let secondPart = String(format: "%02ld", abs(self.remaining.second!))
        
        var stringOut = "\(hourPart):\(minutePart):\(secondPart)"
        if (self.getRemainingSeconds() < 0) {
            stringOut += " (finished)"
        }
        return stringOut
    }
    
    public func getEndTime() -> String {
        let dF = DateFormatter()
        dF.timeStyle = .medium
        
        return dF.string(from: self.endDate)
    }
    
    public mutating func update() {
        if (!active) { return }
        self.remaining = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date(), to: self.endDate)

        if (getRemainingSeconds() == 0 && self.notificationSent == false) {
            self.notificationSent = true
            self.attemptTimerNotification()
            
        }
    }
    
    func attemptTimerNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            if success {
                print("Do a notification!")
                self.doTimerNotification()
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    func doTimerNotification() {
        let un = UNUserNotificationCenter.current()
        un.removePendingNotificationRequests(withIdentifiers: [id.uuidString])
        un.removeDeliveredNotifications(withIdentifiers: [id.uuidString])
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Timer finished"
        notificationContent.sound = UNNotificationSound.default
        
        // thanks Hacking with Swift
        // https://www.hackingwithswift.com/books/ios-swiftui/scheduling-local-notifications

        // choose a random identifier
        let request = UNNotificationRequest(identifier: id.uuidString, content: notificationContent, trigger: nil)

        // add our notification request
        un.add(request)
    }
    
    public mutating func togglePause() {
        if (self.active) {self.pause()}
        else {self.resume()}
    }
    
    public mutating func pause() {
        self.active = false
    }
    
    func getRemainingSeconds() -> Int {
        return Calendar.current.dateComponents([.second], from: Date(), to: self.endDate).second!;
    }
    
    public mutating func resume() {
        // determine how much time was left last time
        var rem = self.remaining.hour! * 3600
        rem += self.remaining.minute! * 60
        rem += self.remaining.second!
        print("Time left on the timer was \(rem)")
        
        
        self.startDate = Date()
        
        if (getRemainingSeconds() > 0) {
            self.endDate = Date().addingTimeInterval(Double(rem))
        }
        
        self.active = true
    }
}
