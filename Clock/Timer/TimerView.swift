//
//  TimerView.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/3/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct TimerView: View {
    @EnvironmentObject var timerHolder : TimerManager
    var body: some View {
        VStack {
            List {
                ForEach(self.timerHolder.timers, id: \.id) { timer in
                    TimerRow(timerItem: timer)
                        .contentShape(Rectangle())
                        .environmentObject(self.timerHolder)
                }
                    .onDelete(perform: self.timerHolder.deleteTimer)
                    .onMove(perform: self.timerHolder.moveTimerItem)
                NewTimerRow()
                        .environmentObject(self.timerHolder)
            }
        }
    }
}

struct TimerView_Previews: PreviewProvider {
    static var previews: some View {
        TimerView()
    }
}
