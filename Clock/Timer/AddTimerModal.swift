//
//  AddTimerModal.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/5/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI
import Combine

struct AddTimerModal: View {
    @EnvironmentObject var timerManager: TimerManager
    @Environment(\.presentationMode) var presentationMode
    
    @State var stringTimerLength = ""
    
    @State var hours: Int = 0
    @State var minutes: Int = 0
    @State var seconds: Int = 0
    
    var numFormatter = NumberFormatter()
    
    init() {
        numFormatter.format = "00"
    }
    var body: some View {
        VStack {
            Text("Set a new timer for")
            HStack {
                Stepper(value: $hours, in: 0...999) {
                    TextField("00", value: $hours, formatter: numFormatter)
                        .frame(minWidth: 40)

                }
                Text(":")
                Stepper(value: $minutes, in: 0...59) {
                    TextField("00", value: $minutes, formatter: numFormatter)
                        .frame(minWidth: 40)
                }
                Text(":")
                Stepper(value: $seconds, in: 0...59) {
                    TextField("00", value: $seconds, formatter: numFormatter)
                        .frame(minWidth: 40)
                }
            }
            
            HStack {
                Button(action: cancel) { Text("Cancel") }
                Button(action: addTimer) { Text("Add Timer") }
            }

        }
    }
    
    func cancel() {
        self.presentationMode.wrappedValue.dismiss()
    }
    
    func addTimer() {
        self.timerManager.addTimer(hours: self.hours, minutes: self.minutes, seconds: self.seconds)
        self.presentationMode.wrappedValue.dismiss()
    }
}

struct AddTimerModal_Previews: PreviewProvider {
    static var previews: some View {
        AddTimerModal()
    }
}
