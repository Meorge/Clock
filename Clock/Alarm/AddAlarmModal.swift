//
//  AddAlarmModal.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/18/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct AddAlarmModal: View {
    @EnvironmentObject var timerManager: TimerManager
    @Environment(\.presentationMode) var presentationMode
    
    @State var alarmItem: AlarmItem = AlarmItem()
    @State var createNewAlarm: Bool = true
    
    var enabledColor = Color.orange
    
    var body: some View {
        Form {
            TextField("Alarm", text: $alarmItem.name)
            Toggle(isOn: $alarmItem.enabled) {
                Text("Active")
            }
            DatePicker("Time:", selection: $alarmItem.date, displayedComponents: .hourAndMinute)
                .labelsHidden()
            HStack {
                SingleCharButton(contentString: "S", set: $alarmItem.days, enumItem: .Sunday, enabledColor: enabledColor)
                SingleCharButton(contentString: "M", set: $alarmItem.days, enumItem: .Monday, enabledColor: enabledColor)
                SingleCharButton(contentString: "T", set: $alarmItem.days, enumItem: .Tuesday, enabledColor: enabledColor)
                SingleCharButton(contentString: "W", set: $alarmItem.days, enumItem: .Wednesday, enabledColor: enabledColor)
                SingleCharButton(contentString: "T", set: $alarmItem.days, enumItem: .Thursday, enabledColor: enabledColor)
                SingleCharButton(contentString: "F", set: $alarmItem.days, enumItem: .Friday, enabledColor: enabledColor)
                SingleCharButton(contentString: "S", set: $alarmItem.days, enumItem: .Saturday, enabledColor: enabledColor)
            }
//            Toggle(isOn: $snoozeEnabled) {
//                Text("Snooze")
//            }
            
            HStack {
                Button(action: cancel) { Text("Cancel") }
                Button(action: createAlarm) { Text("\(self.createNewAlarm ? "Add" : "Edit") Alarm") }
            }

        }
    }
    
    func cancel() {
        self.presentationMode.wrappedValue.dismiss()
    }
    
    func createAlarm() {
        print(alarmItem)
        
        if createNewAlarm {
            self.timerManager.addAlarm(alarmItem)
        } else {
            self.timerManager.updateAlarmDetails(for: alarmItem)
        }
        
        self.presentationMode.wrappedValue.dismiss()
    }
}

struct AddAlarmModal_Previews: PreviewProvider {
    static var previews: some View {
        AddAlarmModal()
    }
}
