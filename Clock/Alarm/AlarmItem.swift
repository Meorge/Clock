//
//  AlarmItem.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/9/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import Foundation
import UserNotifications

public enum DayOfWeek: Int, Codable {
    case Sunday = 1, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
    
    mutating func increment() {
        if self == .Saturday {
            self = .Sunday
            return
        }
        
        self = DayOfWeek(rawValue: self.rawValue + 1)!
        return
    }
}

struct AlarmItem: Codable, Equatable, Hashable {
    var id = UUID()
    
    public var name = "Alarm"
    
    public var date = Date()
    
    public var enabled = true
    
    public var days: Set<DayOfWeek> = [.Sunday, .Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday]
    
    
    
    init() {}
    
    init(newName: String, firstDate: Date) {
        self.name = newName
        self.date = firstDate
        
        self.date = TimerManager.instance.nextAlarmOccurrence(for: self)
//        self.determineNextAlarmOccurrence()
    }
    
    mutating func setDays(newDays: Set<DayOfWeek>) {
        days = newDays
        self.date = TimerManager.instance.nextAlarmOccurrence(for: self)
    }
    
    func getAlarmTimeString() -> String {
        let dF = DateFormatter()
        dF.dateFormat = "hh:mm a"
        return dF.string(from: date)
    }
    
    func getNextFireString() -> String {
        let dF = DateFormatter()
        dF.dateStyle = .medium
        return "Will activate next on \(dF.string(from: date))"
    }
}
