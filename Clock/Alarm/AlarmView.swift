//
//  AlarmView.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/7/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct AlarmView: View {
    @EnvironmentObject var timerHolder : TimerManager
    var body: some View {
        List {
            ForEach(self.timerHolder.alarms, id: \.id) { alarm in
                AlarmRow(alarmItem: alarm)
            }
                .onDelete(perform: self.timerHolder.deleteAlarm)
                .onMove(perform: self.timerHolder.moveAlarmItem)
            NewAlarmRow()
        }
    }
}

struct AlarmView_Previews: PreviewProvider {
    static var previews: some View {
        AlarmView()
    }
}
