//
//  AlarmRow.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/7/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct AlarmRow: View {
    var alarmItem: AlarmItem
    
    @State var editPopoverEnabled: Bool = false
    @EnvironmentObject var timerHolder : TimerManager
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(alarmItem.name)
                    .font(.headline)
                    .fontWeight(.semibold)
                    .foregroundColor(alarmItem.enabled ? Color.primary : Color.secondary)
                Text(alarmItem.getNextFireString())
                    .foregroundColor(alarmItem.enabled ? Color.primary : Color.secondary)
            }
            Spacer()
            HStack(alignment: .firstTextBaseline, spacing: 0) {
                Text(alarmItem.getAlarmTimeString())
                    .font(Font.system(.title).monospacedDigit())
                    .foregroundColor(alarmItem.enabled ? Color.primary : Color.secondary)
            }
        }
        .sheet(isPresented: $editPopoverEnabled) {
            AddAlarmModal(alarmItem: self.alarmItem, createNewAlarm: false)
                .padding(.all, 10.0)
                .environmentObject(self.timerHolder)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            print("haha tap")
            self.editPopoverEnabled.toggle()
        }
    }
}

struct NewAlarmRow: View {
    @State var popoverEnabled: Bool = false
    
    @EnvironmentObject var timerManager: TimerManager
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Add a new alarm...")
                    .font(.headline)
                    .foregroundColor(Color.gray)
                    
                Text("")
            }
            Spacer()
        }
        .sheet(isPresented: $popoverEnabled) {
            AddAlarmModal(alarmItem: AlarmItem(), createNewAlarm: true)
                .padding(.all, 10.0)
                .environmentObject(self.timerManager)
//            AddCountdownModal()
//                .padding(.all, 10.0)
//                .environmentObject(self.timerManager)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            self.popoverEnabled.toggle()
        }
    }
}


struct AlarmRow_Previews: PreviewProvider {
    static var previews: some View {
//        Text("bad")
        AlarmRow(alarmItem: AlarmItem(newName: "Alarm", firstDate: Date()))
    }
}
