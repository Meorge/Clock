//
//  ContentView.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/2/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
    
    @EnvironmentObject var timerHolder : TimerManager
    
    @State var selectKeeper = Set<CountdownItem>()
    
    @State var selectedMode: String? = ""
    
    @State var dateString: String = "---"
    
    @State var selected: Int?
    
    @State var num = 0
    
    
    var body: some View {

        NavigationView {
            List(selection: $selectedMode) {
                NavigationLink(destination: WorldClockView().environmentObject(timerHolder)) {
                    Text("World Clock")
                        .frame(minHeight: 40)
                }
                
                NavigationLink(destination: CountdownView().environmentObject(timerHolder)) {
                    Text("Countdowns")
                        .frame(minHeight: 40)
                }
                .tag("countdown")
                
                NavigationLink(destination: TimerView().environmentObject(timerHolder)) {
                    Text("Timers")
                        .frame(minHeight: 40)
                }
                .tag("timer")
                
                NavigationLink(destination: AlarmView().environmentObject(timerHolder)) {
                    Text("Alarms")
                        .frame(minHeight: 40)
                }
                .tag("alarm")
                
                NavigationLink(destination: StopwatchView().environmentObject(timerHolder)) {
                    Text("Stopwatch")
                        .frame(minHeight: 40)
                }
                .tag("stopwatch")

            }
            .frame(minWidth: 100, idealWidth: 200, maxWidth: 200, maxHeight: .infinity)
            .listStyle(SidebarListStyle())
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(TimerManager.instance)
    }
}
