//
//  SingleCharButton.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/18/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct SingleCharButton: View {
    @State var contentString = "A"
    @Binding var set: Set<DayOfWeek>
    @State var enumItem: DayOfWeek
    @State var enabledColor = Color.green
    @State var disabledColor = Color.gray
    
    var body: some View {
        Button(action: {
            if self.set.contains(self.enumItem) {
                self.set.remove(self.enumItem)
            } else {
                self.set.insert(self.enumItem)
            }
            
        }) {
            Text(contentString)
                .font(.subheadline)
                .frame(width: 35, height: 35)
                .background(set.contains(enumItem) ? enabledColor : disabledColor)
                .clipShape(Circle())
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct SingleCharButton_Previews: PreviewProvider {
    static var previews: some View {
//        SingleCharButton()
        Text("bad")
    }
}
