//
//  StopwatchItem.swift
//  Clock
//
//  Created by Malcolm Anderson on 7/6/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import Foundation

struct StopwatchItem: Codable, Equatable, Hashable {
    var id = UUID()
    
    public var startDate: Date = Date()
    public var endDate: Date = Date()
    public var active: Bool = false
    public var remaining: DateComponents = Calendar.current.dateComponents([.day, .hour, .minute, .second, .nanosecond], from: Date(), to: Date())
    
    public var laps: [LapItem] = [LapItem()]
    
    init() {}
    
    public mutating func start() {
        self.active = true
        self.startDate = Date()
        self.endDate = Date()
        
        self.laps.removeAll()
        var newLapItem = LapItem()
        newLapItem.startDate = Date()
        self.laps.insert(newLapItem, at: 0)
    }
    
    public mutating func stop() {
        self.active = false
    }
    
    public mutating func resume() {
        self.active = true
        
        // find out how long it had been running before...
        let totalSecs = self.getTotalSeconds()

        // ...and add that to the startDate to get the new startDate
        self.startDate = Date().addingTimeInterval(TimeInterval(-totalSecs))
        
        for index in 0...self.laps.count - 1 {
            self.laps[index].resume()
        }
    }
    
    public mutating func clear() {
        self.active = false
        self.startDate = Date()
        self.endDate = Date()
        self.remaining = Calendar.current.dateComponents([.hour, .minute, .second, .nanosecond], from: self.startDate, to: self.endDate)
        
        self.laps.removeAll()
        self.laps.append(LapItem())
    }
    
    public mutating func addLap() {
        var newLapItem = LapItem()
        newLapItem.startDate = Date()
        self.laps.insert(newLapItem, at: 0)
    }
    
    public func getTimeSoFar() -> String {
        let numFormat = "%02ld"
        let milliseconds = self.remaining.nanosecond! / Int(1e+6)
        
        let hourPart = String(format: numFormat, self.remaining.hour!)
        let minutePart = String(format: numFormat, self.remaining.minute!)
        let secondPart = String(format: numFormat, self.remaining.second!)
        let millisecPart = String(format: "%03ld", milliseconds)
        
        let stringOut = "\(hourPart):\(minutePart):\(secondPart).\(millisecPart)"
        return stringOut
    }
    
    public func getLongestLap() -> LapItem {
        var longestLap: LapItem? = nil
        
        for lapItem in self.laps {
            if longestLap == nil || lapItem.getTotalSeconds() > (longestLap?.getTotalSeconds())! {
                longestLap = lapItem
            }
        }
        return longestLap!
    }
    
    public func getShortestLap() -> LapItem {
        var shortestLap: LapItem? = nil
        
        for lapItem in self.laps {
            if shortestLap == nil || lapItem.getTotalSeconds() < (shortestLap?.getTotalSeconds())! {
                shortestLap = lapItem
            }
        }
        return shortestLap!
    }
    
    public func getDifferenceBetweenLapAndPrevLap(lapItem: LapItem) -> String {
        // First, check if the lap array is empty.
        // If so, then just give an empty string.
        if (self.laps.endIndex <= 1) {
            return ""
        }
        
        // Ok, so the lap array isn't empty.
        // We want to find out what the index of *this* lap is first...
        let indexOfThisLap = self.laps.firstIndex(of: lapItem)
        
        // Now that we've got that, we can add one (not subtract!) to get the index of the lap before it
        var indexOfPreviousLap = indexOfThisLap! + 1
        
        // Edge case in case the index of the previous lap is past the end of the array
        if (indexOfPreviousLap >= self.laps.endIndex) {
            indexOfPreviousLap = self.laps.endIndex - 1
        }
        
        // If this lap is the last one in the array (aka the lap listed as Lap 1) then return an empty string -
        // there are no laps before it to compare it to!
        if (indexOfThisLap == self.laps.endIndex - 1) {
            return ""
        }
        
        // Determine the duration of the current lap
        let durationOfThisLap = lapItem.getTotalSeconds()
        
        // ...as well as the duration of the previous lap
        let durationOfPreviousLap = self.laps[indexOfPreviousLap].getTotalSeconds()
        
        // Then simply get the difference between them
        let difference = durationOfThisLap - durationOfPreviousLap
        
        // Create the string version of the difference
        var stringOut = String(format: "%.3f", difference)
        
        // Add a plus sign before the number if it's positive
        if difference >= 0 {
            stringOut = "+\(stringOut)"
        }
        
        // and return it! cool!
        return stringOut
    }
    
    public mutating func update() {
        if (!active) { return }
        
        self.endDate = Date()
        self.remaining = Calendar.current.dateComponents([.hour, .minute, .second, .nanosecond], from: self.startDate, to: self.endDate)
        
  
        self.laps[0].update()
//        if var lastLap = self.laps.last {
//            lastLap.update()
//        }
//        if (self.laps.count == 1) { return }
//
//        for i in 1...self.laps.count - 1 {
//            self.laps[i].update(updateEndDate: false)
//        }
    }
    
    func getTotalSeconds() -> Double {
        return self.endDate.timeIntervalSince(self.startDate)
    }
    
    public func isFresh() -> Bool {
        return self.getTotalSeconds() == 0
    }
}


// each lap item is basically a stopwatch of its own...
// probably gonna just copy the stopwatch code in here
// and only update the top lapitem etc?
struct LapItem: Codable, Equatable, Hashable {
    var id = UUID()
    public var startDate: Date = Date()
    public var endDate: Date = Date()
    public var remaining: DateComponents = Calendar.current.dateComponents([.day, .hour, .minute, .second, .nanosecond], from: Date(), to: Date())
    
    public func getTimeSoFar() -> String {
        let numFormat = "%02ld"
        let milliseconds = self.remaining.nanosecond! / Int(1e+6)
        
        let hourPart = String(format: numFormat, self.remaining.hour!)
        let minutePart = String(format: numFormat, self.remaining.minute!)
        let secondPart = String(format: numFormat, self.remaining.second!)
        let millisecPart = String(format: "%03ld", milliseconds)
        
        let stringOut = "\(hourPart):\(minutePart):\(secondPart).\(millisecPart)"
        return stringOut
    }
    
    func getTotalSeconds() -> Double {
        return self.endDate.timeIntervalSince(self.startDate)
    }
    
    public mutating func resume() {
        // find out how long it had been running before...
        let totalSecs = self.getTotalSeconds()
        
        // ...and subtract that from the current time to fix it
        self.startDate = Date().addingTimeInterval(TimeInterval(-totalSecs))
        self.endDate = Date()
    }
    
    public mutating func update() {
        self.endDate = Date()
        self.remaining = Calendar.current.dateComponents([.hour, .minute, .second, .nanosecond], from: self.startDate, to: self.endDate)
    }
}
