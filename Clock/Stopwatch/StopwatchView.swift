//
//  StopwatchView.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/29/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI
import Combine

struct StopwatchView: View {
    @EnvironmentObject var timerHolder : TimerManager
    var body: some View {
        VStack {
            Spacer()
            Text(self.timerHolder.stopwatchItem.getTimeSoFar())
                .font(Font.system(.largeTitle).monospacedDigit())
            
            HStack {
                Button(action: {
                    if (self.timerHolder.stopwatchItem.active) {
                        self.timerHolder.stopwatchItem.addLap()
                    } else {
                        self.timerHolder.stopwatchItem.clear()
                    }
                }) {
                    Text(self.timerHolder.stopwatchItem.active ? "Lap" : "Reset")
                }
                Button(action: self.timerHolder.toggleStopwatch) {
                    Text(self.timerHolder.stopwatchItem.active ? "Stop" : "Start")
                }
            }
            Spacer()
            List {
                ForEach(self.timerHolder.stopwatchItem.laps, id: \.id) { lapItem in
                    HStack {
                        Text("Lap \(self.timerHolder.stopwatchItem.laps.count - self.timerHolder.stopwatchItem.laps.firstIndex(of: lapItem)!)")
                            .foregroundColor(
                                self.timerHolder.stopwatchItem.laps.count <= 2 ? Color.primary :
                                self.timerHolder.stopwatchItem.getLongestLap() == lapItem ? Color.red :
                                self.timerHolder.stopwatchItem.getShortestLap() == lapItem ? Color.green : Color.primary
                            )
                        Spacer()
                        Text(self.timerHolder.stopwatchItem.getDifferenceBetweenLapAndPrevLap(lapItem: lapItem))
                            .font(Font.system(.subheadline).monospacedDigit())
                            .foregroundColor(
                                Color.gray
                            )
                        Text(lapItem.getTimeSoFar())
                            .font(Font.system(.body).monospacedDigit())
                            .foregroundColor(
                                self.timerHolder.stopwatchItem.laps.count <= 2 ? Color.primary :
                                self.timerHolder.stopwatchItem.getLongestLap() == lapItem ? Color.red :
                                self.timerHolder.stopwatchItem.getShortestLap() == lapItem ? Color.green : Color.primary
                            )
                    }
                }
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
    }
}

struct StopwatchView_Previews: PreviewProvider {
    static var previews: some View {
        StopwatchView()
    }
}
