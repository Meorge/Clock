//
//  CountdownRow.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/2/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI
import Combine

struct CountdownRow: View {
    var countdownItem: CountdownItem
    
    @State var editPopoverEnabled: Bool = false
    
    @EnvironmentObject var timerManager: TimerManager
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(countdownItem.name + "\(countdownItem.inPast() ? " (finished)" : "")")
                    .font(.headline)
                    .fontWeight(.semibold)
                    .foregroundColor(countdownItem.inPast() ? Color.secondary : Color.primary)
                Text(self.timerManager.dateFormatter.string(from: countdownItem.date))
                    .foregroundColor(countdownItem.inPast() ? Color.secondary : Color.primary)
            }
            Spacer()
            CountdownWidget(item: countdownItem).environmentObject(self.timerManager)
                .foregroundColor(countdownItem.inPast() ? Color.secondary : Color.primary)
        }
        .sheet(isPresented: $editPopoverEnabled) {
            AddCountdownModal(countdownItem: self.countdownItem, createNewCountdown: false)
                .padding(.all, 10.0)
                .environmentObject(self.timerManager)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            print("haha tap")
            self.editPopoverEnabled.toggle()
        }
    }
}

struct NewCountdownRow: View {
    @State var str: String
    @State var newCountdownPopoverEnabled: Bool = false
    
    @EnvironmentObject var timerManager: TimerManager
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Add a new countdown...")
                    .font(.headline)
                    .foregroundColor(Color.gray)
                    
                Text("")
            }
            Spacer()
        }
        .sheet(isPresented: $newCountdownPopoverEnabled) {
            AddCountdownModal()
                .padding(.all, 10.0)
                .environmentObject(self.timerManager)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            self.newCountdownPopoverEnabled.toggle()
        }
    }
}

struct CountdownRow_Previews: PreviewProvider {
    static var previews: some View {
        Text("swiftui is bad")
//        CountdownRow(countdownItem: CountdownItem(name: "its a countdown", date: Date(timeIntervalSince1970: 1596265200), id: UUID())).environmentObject(TimerManager())
    }
}
