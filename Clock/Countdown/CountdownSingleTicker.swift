//
//  CountdownSingleTicker.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/2/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct CountdownSingleTicker: View {
    var units: Calendar.Component
    var item: CountdownItem
    
    @State var str = "--"
    @EnvironmentObject var timerHolder : TimerManager
    
    func getString() -> String {
        switch units {
            case .day:
                return self.item.daysRemaining()
            case .hour:
                return self.item.hoursRemaining()
            case .minute:
                return self.item.minutesRemaining()
            case .second:
                return self.item.secondsRemaining()
            default:
                return "??"
        }
    }
    var body: some View {
        VStack(alignment: .center) {
            Text(self.getString())
                .font(Font.system(.title).monospacedDigit())
                .frame(minWidth: 0, maxWidth: .infinity)
            Text(self.timerHolder.getUnitString(unit: units))
                .font(.caption)
                .multilineTextAlignment(.center)
            
        }
        .frame(width: 60)
    }
}

struct CountdownSingleTicker_Previews: PreviewProvider {
    static var previews: some View {
//        CountdownSingleTicker(units: Calendar.Component.day, date: Date())
        Text("swiftui is bad")
    }
}
