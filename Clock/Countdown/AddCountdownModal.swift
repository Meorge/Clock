//
//  AddCountdownModal.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/2/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct AddCountdownModal: View {
    @State var eventName: String = ""
    @State var date: Date = Date()
    @State var countdownItem: CountdownItem = CountdownItem()
    @State var createNewCountdown: Bool = true
    
    @Environment(\.presentationMode) var presentationMode
    
    @EnvironmentObject var manager : TimerManager
    
    var body: some View {
        VStack(alignment: .center) {
            TextField("Event name", text: $eventName)
                .font(.headline)
                .border(Color.clear)
                .onAppear {
                    self.eventName = self.countdownItem.name
                }
            DatePicker(selection: $date, label: {Text("")})
                .frame(minWidth: 0, maxWidth: .infinity)
                .onAppear {
                    self.date = self.countdownItem.date
                }
            
            Picker(selection: $countdownItem.pastBehavior, label: Text("After hitting zero:")) {
                Text("Restrict to zero").tag(CountdownPastBehavior.ShowZero)
                Text("Show time elapsed").tag(CountdownPastBehavior.ShowTimeElapsed)
                Text("Reschedule").tag(CountdownPastBehavior.Repeat)
            }
            
            if self.countdownItem.pastBehavior == .Repeat {
                Picker(selection: $countdownItem.repeatType, label: Text("for")) {
                    Text("next year").tag(CountdownRepetitionType.Yearly)
                    Text("next month").tag(CountdownRepetitionType.Monthly)
                    Text("next week").tag(CountdownRepetitionType.Weekly)
                    Text("tomorrow").tag(CountdownRepetitionType.Daily)
                }
            }
            
            HStack {
            Button(action: cancel) {
                Text("Cancel")
            }
            Button(action: addCountdown) {
                Text("\(self.createNewCountdown ? "Add" : "Edit") Countdown")
            }
            }
        }
    }
    
    func cancel() {
        self.presentationMode.wrappedValue.dismiss()
    }
    
    func addCountdown() {
        if (self.createNewCountdown) {
            self.manager.addCountdown(item: CountdownItem(countdownName: self.eventName, countdownDate: self.date))
        } else {
            var newCountdown = self.countdownItem
            newCountdown.name = self.eventName
            newCountdown.date = self.date
            self.manager.updateCountdownDetails(original: self.countdownItem, new: newCountdown)
        }
        self.presentationMode.wrappedValue.dismiss()
    }
}

struct AddCountdownModal_Previews: PreviewProvider {
    static var previews: some View {
        AddCountdownModal()
    }
}
