//
//  CountdownView.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/3/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI

struct CountdownView: View {
    @EnvironmentObject var timerHolder : TimerManager
    @State var selectKeeper = Set<CountdownItem>()
    
    var body: some View {
        VStack {
            List(selection: $selectKeeper) {
                ForEach(self.timerHolder.countdowns, id: \.id) { countdown in
                    CountdownRow(countdownItem: countdown)
                        .contentShape(Rectangle())
                        .environmentObject(self.timerHolder)
                }
                    .onDelete(perform: self.timerHolder.deleteCountdown)
                    .onMove(perform: self.timerHolder.moveCountdown)
                NewCountdownRow(str: "")
                    .environmentObject(self.timerHolder)
            }
        }
    }
}

struct CountdownView_Previews: PreviewProvider {
    static var previews: some View {
        CountdownView().environmentObject(TimerManager.instance)
    }
}
