//
//  CountdownWidget.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/2/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import SwiftUI
import Combine

struct CountdownWidget: View {
    var paddingForColons: CGFloat = 20.0
    
    var item: CountdownItem
    
    @EnvironmentObject var timerManager : TimerManager
    
    var body: some View {
        HStack {
            CountdownSingleTicker(units: Calendar.Component.day, item: item).environmentObject(self.timerManager)
            Text(":")
                .font(.title)
                .frame(width: 20)
                .padding(.bottom, paddingForColons)
            CountdownSingleTicker(units: Calendar.Component.hour, item: item).environmentObject(self.timerManager)
            Text(":")
                .font(.title)
                .frame(width: 20)
                .padding(.bottom, paddingForColons)
            CountdownSingleTicker(units: Calendar.Component.minute, item: item).environmentObject(self.timerManager)
            Text(":")
                .font(.title)
                .frame(width: 20)
                .padding(.bottom, paddingForColons)
            CountdownSingleTicker(units: Calendar.Component.second, item: item).environmentObject(self.timerManager)
        }
    }
}
//
//struct CountdownWidget_Previews: PreviewProvider {
//    static var previews: some View {
////        CountdownWidget(date: Date())
//        Text("swiftui is bad")
//    }
//}
