//
//  CountdownItem.swift
//  Clock
//
//  Created by Malcolm Anderson on 6/2/20.
//  Copyright © 2020 Malcolm Anderson. All rights reserved.
//

import Foundation

enum CountdownPastBehavior: Int, Codable {
    case ShowZero = 0, ShowTimeElapsed, Repeat
}

enum CountdownRepetitionType: Int, Codable {
    case Yearly = 0, Monthly, Weekly, Daily
}
struct CountdownItem: Codable, Equatable, Hashable {
    public var name: String = "Countdown"
    public var date: Date = Date()
    public var pastBehavior = CountdownPastBehavior.ShowTimeElapsed
    public var repeatType = CountdownRepetitionType.Yearly
    
    
    
    public var remaining: DateComponents = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date(), to: Date())
    
    public var id = UUID()
    
//    static func ==(lhs: CountdownItem, rhs: CountdownItem) -> Bool {
//        return lhs.id == rhs.id
//    }
    
    init() {
        self.name = "Countdown"
        self.date = Date()
        self.update()
    }
    
    init(countdownName: String, countdownDate: Date) {
        self.name = countdownName
        self.date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: countdownDate))!
        self.update()
    }
    
    mutating func update() {
        if inPast() && pastBehavior == .Repeat {
            var dC = DateComponents()
            
            switch repeatType {
            case .Yearly:
                dC.year = 1
            case .Monthly:
                dC.month = 1
            case .Weekly:
                dC.weekOfYear = 1
            case .Daily:
                dC.day = 1
            }
            
            self.date = Calendar.current.date(byAdding: dC, to: self.date)!
        }
        
        self.remaining = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date(), to: self.date)
    }
    
    func inPast() -> Bool {
        return Date() > date
    }
    
    func numsRemaining(for num: Int) -> Int {
        var num = num
        switch pastBehavior {
            case .ShowZero:
                num = max(num, 0)
            default:
                num = abs(num)
        }
        
        return num
    }
    
    func daysRemaining() -> String {
        return timeItemRemaining(for: numsRemaining(for: self.remaining.day!))
    }
    
    func hoursRemaining() -> String {
        return timeItemRemaining(for: numsRemaining(for: self.remaining.hour!))
    }
    
    func minutesRemaining() -> String {
        return timeItemRemaining(for: numsRemaining(for: self.remaining.minute!))
    }
    
    func secondsRemaining() -> String {
        return timeItemRemaining(for: numsRemaining(for: self.remaining.second!))
    }
    
    func timeItemRemaining(for item: Int) -> String {
        return String(format: "%02ld", item)
    }
}
